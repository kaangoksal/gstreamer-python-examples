import sys
import os

if sys.version_info[0] < 3:
    import Tkinter as tkinter
else:
    import tkinter

import gi

gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject

# Needed for set_window_handle():
gi.require_version('GstVideo', '1.0')
from gi.repository import GstVideo


def set_frame_handle(bus, message, frame_id):
    print("Message ", message)
    print("Frame Id  ", frame_id)
    print("Bus ", bus)
    if not message.get_structure() is None:
        if message.get_structure().get_name() == 'prepare-window-handle':
            display_frame = message.src
            display_frame.set_property('force-aspect-ratio', True)
            display_frame.set_window_handle(frame_id)


window = tkinter.Tk()
window.title("Multiple videos in a column using Tk and GST 1.0")
window.geometry('480x480')

Gst.init(None)
GObject.threads_init()

display_frame = tkinter.Frame(window, bg='')
display_frame.configure(height=400, width=400, padx=10, pady=10)
display_frame.grid(column=0,row=0)
display_frame.grid_propagate(0)

#display_frame.place(relx=0, rely=0,anchor=tkinter.NW, relwidth=1, relheight=1)

# button = tkinter.Button(window,text="Test")
# button.place(relx=0.5, rely=0.6, anchor=tkinter.SE)

frame_id = display_frame.winfo_id()

#player = Gst.parse_launch("videotestsrc! videoconvert ! videotestsrc ! videoconvert ! autovideosink) # MACOS TODO AUTO HANDLE THIS
player = Gst.parse_launch("videotestsrc ! videoconvert ! xvimagesink")
print(type(player))
player.set_state(Gst.State.PLAYING)

bus = player.get_bus()
bus.enable_sync_message_emission()
bus.connect('sync-message::element', set_frame_handle, frame_id)

window.mainloop()
